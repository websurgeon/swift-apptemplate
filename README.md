# Swift App Template #

A template Xcode project that provides the starting structure for an app.
There are tags for each different type if started app (e.g. with or without dependency management, Carthage or Cocoapods).

### Available Templates ###
Checkout using one of the following tags ($ get checkout <tag>):

* **EmptyApp**

    *A blank app*

* **EmptyAppAutoBuildNumber**

    *Same as EmptyApp with a build phase script to automatically set the build number and app display name*

* **EmptyAppSharedDevScheme**

    *Same as EmptyAppAutoBuildNumber with a shared 'Dev' scheme*

* **EmptyCarthageApp**

    *Same as EmptyAppSharedDevScheme with a setup script to use Carthage dependency management*

* **EmptyCarthageReactiveApp**

    *Same as EmptyCarthageApp with a ReactiveKit framework already included*

* **EmptyCocoaPodsApp**

    *Same as EmptyAppSharedDevScheme with a setup script to use Cocoapods dependency management*

* **EmptyCocoaPodsReactiveApp**

    *Same as EmptyCocoaPodsApp with a ReactiveKit framework already included*

### Getting started ###

After cloning the repo, simply checkout the tag for the required template.
Once checked out, if the template includes a 'setup' file, you can alter the Cartfile/Podfile as required and run setup

    $ ./setup`

OR

    $ sh setup


